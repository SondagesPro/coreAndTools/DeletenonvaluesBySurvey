<?php
/**
 * deletenonvaluesBySurvey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2022 Denis Chenu <http://www.sondages.pro>
 * @license GPL
 * @version 0.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class DeletenonvaluesBySurvey extends PluginBase
{

    static protected $name = 'DeletenonvaluesBySurvey';
    static protected $description = 'LimeSurvey can delete or not answers of questions hidden by relevance. You can choose globally the solution, this plugin offer to choose by survey';

    protected $storage = 'DbStorage';

    /**
    * @var array[] the settings
    */
    protected $settings = array(
        'deletenonvalues' => array(
            'type' => 'select',
            'label'=>'LimeSurvey delete answers of questions hidden by relevance',
            'options'=>array(
                1 =>"Yes",
                0 =>"No",
            ),
            'htmlOptions'=>array(
                'empty' => "Use default from config.php",
            ),
            'default'=>"",
        ),
    );

    public function init()
    {
        /* Survey settings */
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
        /* In survey */
        $this->subscribe('beforeSurveyPage');
    }

    /** @inheritdoc **/
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        $currentDefault = $this->get('deletenonvalues',null,null,"");
        if ($currentDefault) {
            $currentDefault = Yii::app()->getConfig('deletenonvalues');
        }
        $defaultString = $currentDefault ? gT('Yes') : gT('No');
        $oEvent->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'deletenonvalues'=>array(
                    'type'=>'select',
                    'label'=>$this->translate("Delete answers of irrelevant questions."),
                    'help' =>sprintf(
                        $this->translate("Choose if LimeSurvey save and keep responses to conditional questions. Get more details on %sLimeSurvey manual%s “deletenonvalues” option."),
                        "<a href='https://manual.limesurvey.org/Optional_settings#Survey_behavior' rel='external'>",
                        "</a>"
                    ),
                    'options'=>array(
                        1 =>gT("Yes"),
                        0 =>gT("No"),
                    ),
                    'htmlOptions'=>array(
                        'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"),$defaultString)),
                    ),
                    'current'=>$this->get('deletenonvalues','Survey',$oEvent->get('survey'),"")
                ),
            )
        ));
    }

    /** @inheritdoc **/
    public function getPluginSettings($getValues=true)
    {
        if(!Permission::model()->hasGlobalPermission('settings','read')) {
            throw new CHttpException(403);
        }
        $defaultString = Yii::app()->getConfig('deletenonvalues') ? gT('Yes') : gT('No');
        $this->settings['deletenonvalues']['label'] = $this->translate('Delete answers of irrelevant questions.');
        $this->settings['deletenonvalues']['options'] = array( 1 => gT("Yes"), 0 => gT("No") );
        $this->settings['deletenonvalues']['htmlOptions']['empty'] = CHtml::encode(
            sprintf($this->translate("Use default from config.php (%s)"),
                $defaultString
            )
        );
        $this->settings['deletenonvalues']['help'] = sprintf(
            $this->translate("Choose if LimeSurvey save and keep responses to conditional questions. Get more details on %sLimeSurvey manual%s"),
            "<a href='https://manual.limesurvey.org/Optional_settings/en#Survey_behavior' rel='external'>",
            "</a>"
        );
        return parent::getPluginSettings($getValues);
    }

    /** @inheritdoc **/
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    public function beforeSurveyPage() {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $deletenonvalues = $this->get('deletenonvalues','Survey',$this->event->get('surveyId'), "");
        if ($deletenonvalues === '') {
            $deletenonvalues = $this->get('deletenonvalues',null,null,"");
        }
        if($deletenonvalues !=="") {
            Yii::app()->setConfig('deletenonvalues',$deletenonvalues);
        }
    }

    /**
    * get translation
    * @param string
    * @return string
    */
    private function translate($string){
        return Yii::t('',$string,array(),'Messages'.get_class($this));
    }

    /**
    * Add this translation just after loaded all plugins
    * And set the config if needed
    * @see event afterPluginLoad
    */
    public function afterPluginLoad(){
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        // messageSource for this plugin:
        $messageSource=array(
            'class' => 'CGettextMessageSource',
            'cacheID' => get_class($this).'Lang',
            'cachingDuration'=>3600,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR.'locale',
            'catalog'=>'messages',// default from Yii
        );
        Yii::app()->setComponent('Messages'.get_class($this),$messageSource);
        if($deletenonvalues = $this->get('deletenonvalues',null,null,"") !== "") {
            Yii::app()->setConfig('deletenonvalues',$deletenonvalues);
        }
    }
}
