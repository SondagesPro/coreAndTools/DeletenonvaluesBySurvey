# DeletenonvaluesBySurvey

LimeSurvey may or may not suppress answers (set to NULL)  to irrelevant questions with a parameter update via a PHP file. Get more detail on this system at [LimeSurvey Manual](https://manual.limesurvey.org/Optional_settings/en#Survey_behavior).

This plugin allows you to update these settings on the administration GUI and choose the behavior for each surveys$

## LimeSurvey support

This plugin was built and tested on LimeSurvey version 3.X . Can support versions up to 2.6 with exception of 2.61 version.

## Installation

- Go to your LimeSurvey plugin Directory
- Clone in plugins/DeletenonvaluesBySurvey directory `git clone https://gitlab.com/SondagesPro/coreAndTools/DeletenonvaluesBySurvey.git DeletenonvaluesBySurvey`
- You can download the zip at <http://dl.sondages.pro/DeletenonvaluesBySurvey.zip>

## Plugin Configuration

- Globally : Just choose if you activate deletenonvalues globally or keep default.
- By Survey : Just choose if you activate deletenonvalues on each survey or keep default.

## Home page & Copyright
- HomePage <https://www.sondages.pro/>
- Copyright © 2019-2021 Denis Chenu <http://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
